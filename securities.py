import numpy as np
import pandas as pd
from typing import *
from datetime import datetime


def is_number(s: Any) -> bool:
    """Check if the price is a numeric

    :param s:
    :return: True if s is numeric, False otherwise
    """
    try:
        float(s)
        return True
    except ValueError:
        return False
    except TypeError:
        return False


def test():
    assert is_number("4")
    assert not is_number("a")
    assert not is_number("")
    assert not is_number(None)
    assert not is_number("abc")


if __name__ == '__main__':

    test()

    df = pd.read_csv('C:/Studies/self study/python/data/Benchmark Issues.csv')
    df.columns = ['year', 'month', 'day', 'price']

    # filter out rows with Nan price values
    df = df.loc[pd.notnull(df['price'])]

    # filter out rows with string price values
    df = df.loc[df['price'].apply(is_number)]

    df.reset_index(inplace=True, drop=True)

    # set year, month information
    for i in range(len(df['year'])):
        if i != 0 and pd.isnull(df.loc[i, 'year']):
            df.loc[i, 'year'] = df.loc[i - 1, 'year']

        if i != 0 and pd.isnull(df.loc[i, 'month']):
            df.loc[i, 'month'] = df.loc[i - 1, 'month']

    df['day'] = df['day'].astype(int)
    df['month'] = df['month'].str.upper()
    df['date'] = df['year'] + '-' + df['month'] + '-' + df['day'].astype(str)
    df['date'] = pd.to_datetime(df['date'], format='%Y-%b-%d')

    df['price'] = df['price'].astype(float)
    df['return'] = [0] + (df['price'].values[1:] - df['price'].values[:-1]).tolist()

    # only keep most recent 8 years of records
    dt = datetime.now()
    dt = dt.replace(year=dt.year-8)
    df = df[df['date'].apply(lambda x: x >= dt)]
    df.reset_index(inplace=True, drop=True)

    df['weekday'] = df['date'].apply(lambda x: x.weekday())

    # separate df to two parts - monday data(mondayDF) and non-monday data(non_monday_df)
    non_monday_df = df[df['weekday'] != 0]

    mondayDF = df[df['weekday'] == 0]

    # randomize the index for monday data
    mondayDF.index = np.random.permutation(mondayDF.index)

    # concatenate monday and non-monday data, and sort by index
    df = pd.concat([mondayDF, non_monday_df]).sort_index()

    df.to_csv("data/result.csv")
